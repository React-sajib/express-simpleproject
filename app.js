const express = require('express')
const morgan  = require('morgan')

//here is all router
const UserRouter = require('./Router')
const PostRoute = require('./postRouter')

const APP =express()

// APP.use(morgan('dev'))


//create All router 
APP.use('/user' , UserRouter)
APP.use('/posts' , PostRoute)






function customMiddleware (req,res,next){
    if(req.url == '/contact'){
        res.send('<h1>this page is blocked</h1>')
    }
    next()
}
function customMiddleware (req,res,next){
    if(req.url == '/contact'){
        res.send('<h1>this page is blocked</h1>')
    }
    next()
}

function tinilogger (){
    return(req,res,next)=>{
        console.log(` ${req.url}`)
        console.log(`${res}`)
        next()
    }
}

const middlewares = [customMiddleware,tinilogger()]

APP.use(middlewares)




APP.get('/about',morgan('dev'),(req,res)=>{
    // console.log(res,'ata about')
    res.send('<h1>this is about</h1>')
})

APP.get('/portfolio',(req,res)=>{
    res.send('<h1>this is portfolio</h1>')
})

APP.get('/contact',(req,res)=>{
    res.send('<h1>this is contact</h1>')
})


APP.get('/',(req,res)=>{
    res.send('<h1>this is javascript</h1>')
})
APP.get('*',(req,res)=>{
    res.send('<h1>No page found</h1>')
})


const PORT = process.env.PORT || 8080

APP.listen(PORT,()=>{
    console.log(`Example app listening at http://localhost:${PORT}`)
})


