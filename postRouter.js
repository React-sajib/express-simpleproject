
const express = require('express')
const Router = express.Router()
const { Allposts,
        Createposts,
        Singleposts,
        Updateposts,
        DeletePosts } = require('./postController/PostController') 

Router.get('/',Allposts)//get all post

Router.post('/',Createposts)//create new post

Router.get('/product/:postid',Singleposts)//show single post

Router.put('/:postid',Updateposts) //update single post

Router.delete('/:postid',DeletePosts)


module.exports = Router